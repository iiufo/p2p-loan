package com.vanguard.p2p.base.vo;


import com.vanguard.p2p.base.domain.Organization;
import com.vanguard.p2p.base.domain.User;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * @author vanguard
 * @version 1.0
 * @descripe
 * @date: 2018-02-09
 */
@Setter
@Getter
public class OrganizationVO extends Organization {

	private static final long serialVersionUID = 1L;
	
	/** 管理员列表 */
	private List<User> users;
	
	/** 管理员数量 */
	private Integer numberUser;

}
