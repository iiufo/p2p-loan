package com.vanguard.p2p.base.domain;

import com.vanguard.p2p.base.BaseDomain;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * @author vanguard
 * @version 1.0
 * @descripe Organization / 组织表 实体类
 * @date: 2018-02-09
 */
@Setter
@Getter
public class Organization extends BaseDomain {

    /**
     * 组织名称
     */
	private String organizationName;
    /**
     * 系统数据 1=是,只有超级管理员能修改/0=否,拥有角色修改人员的权限能都修改
     */
	private Integer isSystem;
    /**
     * 状态 0=冻结/1=正常
     */
	private Integer status;
    /**
     * 创建时间
     */
	private Date createTime;
    /**
     * 创建者
     */
	private String createBy;
    /**
     * 更新时间
     */
	private Date updateTime;
    /**
     * 更新者
     */
	private String updateBy;
    /**
     * 备注信息
     */
	private String remarks;

}
