package com.vanguard.p2p.system.service;


import com.github.pagehelper.PageInfo;
import com.vanguard.p2p.base.domain.Organization;
import com.vanguard.p2p.base.vo.OrganizationVO;
import com.vanguard.p2p.pojo.P2PResult;
import com.vanguard.p2p.pojo.PageResult;

import java.util.List;

/**
 * @author vanguard
 * @version 1.0
 * @descripe Organization / 组织表 业务逻辑层接口
 * @date: 2018-02-09
 */
public interface IOrganizationService {
	
	/**
	 * 创建组织
	 * @param organization 组织信息
	 * @param userName 操作人
	 * @return
	 */
	P2PResult insertOrganization(Organization organization, String userName);
	
	/**
	 * 根据组织状态查找组织列表
	 * @param status 组织状态
	 * @return List<Organization>
	 */ 
	List<Organization> listBySataus(Integer status);
	
	/**
	 * 根据分页信息/搜索内容查找组织列表
	 * @param pageInfo 分页信息
	 * @param search 搜索内容
	 * @return
	 */
	PageResult<Organization> listByPage(PageInfo pageInfo, String search);
	
	/**
	 * 查找组织及其组织人员
	 * @return
	 */
	List<OrganizationVO> listOrganizationsDetail();
	
	/**
	 * 更新组织状态
	 * @param organizationId 组织ID
	 * @return
	 */
	P2PResult updateStatus(Long organizationId);
	
	/**
	 * 更新组织信息
	 * @param organization 组织信息
	 * @param userName 操作人
	 * @return
	 */
	P2PResult updateOrganization(Organization organization, String userName);
	
	/**
	 * 根据组织ID删除组织信息,同时重置用户表组织ID
	 * @param organizationId
	 * @return
	 */
	P2PResult deleteByOrganizationId(Long organizationId);
}
