package com.vanguard.p2p.business.service.impl;

import com.vanguard.p2p.base.vo.UserVO;
import com.vanguard.p2p.business.domain.Member;
import com.vanguard.p2p.business.domain.MemberLoginLog;
import com.vanguard.p2p.business.service.IUserService;
import com.vanguard.p2p.exception.ValidateException;
import com.vanguard.p2p.pojo.P2PResult;
import org.springframework.stereotype.Service;

/**
 * @author vanguard
 * @version 1.0
 * @describe
 * @date 2018/02/12
 */
@Service
public class UserServiceImpl implements IUserService {

    @Override
    public P2PResult register(Member user) throws ValidateException {
        return null;
    }

    @Override
    public Member getByUserName(String userName) {
        return null;
    }

    @Override
    public Member getByEmail(String email) {
        return null;
    }

    @Override
    public UserVO getById(Long userId) {
        return null;
    }

    @Override
    public Integer updateLogById(Long memId, MemberLoginLog memberLoginLog) {
        return null;
    }

    @Override
    public P2PResult updateEmailActive(String email) {
        return null;
    }

    @Override
    public Integer getEmailIsActive(Long userId) {
        return null;
    }

    @Override
    public P2PResult updatePerfectUser(String email, String telephone, String realName) throws ValidateException {
        return null;
    }

    @Override
    public P2PResult updatePasswordByEmail(String loginPassword, String email) {
        return null;
    }
}
