package com.vanguard.p2p.controller;

import com.vanguard.p2p.base.BaseController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @author vanguard
 * @version 1.0
 * @describe
 * @date 2018/02/23
 */
@Controller
public class IndexController extends BaseController {

    @GetMapping("/index")
    public String index() {
        return "index";
    }

}
