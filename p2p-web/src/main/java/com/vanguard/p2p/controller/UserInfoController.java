package com.vanguard.p2p.controller;

import com.vanguard.p2p.base.BaseController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author vanguard
 * @version 1.0
 * @describe 个人中心表现层控制器
 * @date 2018/02/23
 */
@Controller
@RequestMapping("/uc/user")
public class UserInfoController extends BaseController {
    /**
     * 个人中心首页
     * @return
     */
    @RequestMapping("/portal")
    public String portal() {
        return "member/main";
    }
}
