package com.vanguard.p2p.controller;

import com.vanguard.p2p.base.BaseController;
import com.vanguard.p2p.business.domain.MemberLoginLog;
import com.vanguard.p2p.business.service.IUserService;
import com.vanguard.p2p.constant.CommonReturnCode;
import com.vanguard.p2p.constant.UserReturnCode;
import com.vanguard.p2p.pojo.P2PResult;
import com.vanguard.p2p.utils.RSAUtils;
import com.vanguard.p2p.utils.ServletUtils;
import com.vanguard.p2p.utils.SingletonLoginUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.*;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Date;

/**
 * @author vanguard
 * @version 1.0
 * @describe 用户登录、注册表现层
 * @date 2018/02/23
 */
@Controller
@RequestMapping("/pass")
public class UserController extends BaseController {

    @Autowired
    private IUserService userService;
    /**
     * 用户登录界面
     * @return
     */
    @GetMapping(value = "/login")
    public String login() {
        return "user/login";
    }

    /**
     * 用户登录
     * @param userName
     * @param password
     * @return
     */
    @PostMapping(value = "/login")
    public Object login(String userName, String password) {
        // 服务器端，使用RSAUtils工具类对密文进行解密
        String passWord = RSAUtils.decryptStringByJs(password);

        Subject currentUser = SecurityUtils.getSubject();
        UsernamePasswordToken token = new UsernamePasswordToken(userName, passWord);
        // 默认不记住我
        token.setRememberMe(false);
        try{
            currentUser.login(token);
            MemberLoginLog userLoginLog = new MemberLoginLog(new Date(), ServletUtils.getIpAddr(),
                    SingletonLoginUtils.getUserId(), ServletUtils.getUserOperatingSystem(),
                    ServletUtils.getUserBrowser());
            Integer count = userService.updateLogById(SingletonLoginUtils.getUserId(), userLoginLog);
            return new P2PResult(CommonReturnCode.SUCCESS, count);
        } catch (UnknownAccountException e) {
            logger.error(UserReturnCode.USER_NOT_EXIST.getMessage(), e);
            return new P2PResult(UserReturnCode.USER_NOT_EXIST);
        } catch (DisabledAccountException e) {
            logger.error(UserReturnCode.USER_SUSPEND.getMessage(), e);
            return new P2PResult(UserReturnCode.USER_SUSPEND);
        } catch (IncorrectCredentialsException e) {
            logger.error(UserReturnCode.WRONG_PASSWORD.getMessage(), e);
            return new P2PResult(UserReturnCode.WRONG_PASSWORD);
        } catch (ExcessiveAttemptsException e) {
            logger.error(UserReturnCode.ACCOUNT_LOCK.getMessage(), e);
            return new P2PResult(UserReturnCode.ACCOUNT_LOCK);
        }catch (RuntimeException e) {
            logger.error(CommonReturnCode.UNKNOWN_ERROR.getMessage(), e);
            return new P2PResult(CommonReturnCode.UNKNOWN_ERROR);
        }
    }

    /**
     * 用户注册界面
     * @return
     */
    @GetMapping(value = "/register")
    public String register() {
        return "user/register";
    }
}
