package com.vanguard.p2p.cache.impl;

import com.vanguard.p2p.cache.JedisManager;
import com.vanguard.p2p.cache.JedisShiroCache;
import com.vanguard.p2p.cache.ShiroCacheManager;
import org.apache.shiro.cache.Cache;

/**
 * @author vanguard
 * @version 1.0
 * @descripe JRedis管理
 * @date: 2018-02-09
 */
public class JedisShiroCacheManager implements ShiroCacheManager {

    private JedisManager jedisManager;

    @Override
    public <K, V> Cache<K, V> getCache(String name) {
        return new JedisShiroCache<K, V>(name, getJedisManager());
    }

    @Override
    public void destroy() {
    	//如果和其他系统，或者应用在一起就不能关闭
    	//getJedisManager().getJedis().shutdown();
    }

    public JedisManager getJedisManager() {
        return jedisManager;
    }

    public void setJedisManager(JedisManager jedisManager) {
        this.jedisManager = jedisManager;
    }
}
