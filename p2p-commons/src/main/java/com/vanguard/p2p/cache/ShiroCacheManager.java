package com.vanguard.p2p.cache;

import org.apache.shiro.cache.Cache;

/**
 * @author vanguard
 * @version 1.0
 * @descripe shiro cache manager 接口
 * @date: 2018-02-09
 */
public interface ShiroCacheManager {

    <K, V> Cache<K, V> getCache(String name);

    void destroy();

}
