package com.vanguard.p2p.pojo;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @author vanguard
 * @version 1.0
 * @descripe AuthorizingUser 用户登录信息实体类
 * @date: 2018-02-08
 */
@Setter
@Getter
public class AuthorizingUser implements Serializable {

	private static final long serialVersionUID = 1L;
	
	/** 用户ID*/
	private Long userId;
	
	/** 用户编号 */
	private Long userNumber;

	/** 昵称 */
	private String userName;

	/** 真实姓名 */
	private String realName;
	
    /** 电子邮箱*/
	private String email;
	
    /** 手机号码*/
	private String telephone;
	
	/** 加密密码的盐 */
	private String salt;

	/** 证书凭证 */
	public String getCredentialsSalt() {
		if (userNumber != null && salt != null) {
			return userNumber + salt;
		}
		return null;
	}
}
