package com.vanguard.p2p.security;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * 
* 项目名称：morning-cms-web Maven Webapp   
* 类名称：AuthorizingUser   
* 类描述：SystemAuthorizingUser 管理员登录信息实体类   
* 创建人：陈星星   
* 创建时间：2017年4月1日 下午4:30:31   
*
 */
@Setter
@Getter
public class AuthorizingUser implements Serializable {

	private static final long serialVersionUID = 1L;
	
	/** 管理员ID */
	private Long userId;

	/** 登录名 */
	private String userName;

	/** 真实姓名 */
	private String realName;
	
	/** 加密密码的盐 */
	private String salt;

	
	/** 证书凭证 */
	public String getCredentialsSalt() {
		if (userName != null && salt != null) {
			return userName + salt;
		}
		return null;
	}
}
